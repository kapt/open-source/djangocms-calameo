BOLD := \033[1m
RESET := \033[0m

update:
	curl --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects/17984557/repository/files/Makefile/raw?ref=master" > Makefile

update_precommit:
	curl --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects/17984557/repository/files/.pre-commit-config.yaml/raw?ref=master" > .pre-commit-config.yaml


.PHONY: lint
lint:  ## Run all linters (check-isort, check-black, flake8)
lint: check-isort check-black flake8

.PHONY: check-isort
check-isort:  ## Run the isort tool in check mode only (won't modify files)
	@echo "$(BOLD)Checking isort(RESET)"
	poetry run isort --check-only `find . -name .venv -prune -o -name '*.py' -print`

.PHONY: check-black
check-black:  ## Run the black tool in check mode only (won't modify files)
	@echo "$(BOLD)Checking black$(RESET)"
	poetry run black --check `find . -name .venv -prune -o -name '*.py' -print`

.PHONY: flake8
flake8:  ## Run the flake8 tool
	@echo "$(BOLD)Running flake8$(RESET)"
	poetry run flake8 `find . -name .venv -prune -o -name '*.py' -print`

.PHONY: pretty
pretty:  ## Run all code beautifiers (isort, black)
pretty: isort black

.PHONY: isort
isort:  ## Run the isort tool and update files that need to
	@echo "$(BOLD)Running isort$(RESET)"
	poetry run isort `find . -name .venv -prune -o -name '*.py' -print`

.PHONY: black
black:  ## Run the black tool and update files that need to
	@echo "$(BOLD)Running black$(RESET)"
	poetry run black `find . -name .venv -prune -o -name '*.py' -print`
