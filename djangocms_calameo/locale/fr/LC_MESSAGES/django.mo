��    !      $  /   ,      �     �     �     �               /     <  +   Z     �     �     �     �     �     �     �  (   �     �            "   )     L  #   S     w     |     �     �     �     �     �     �     �  	   �  A  �          &  !   +     M     S     c  #   t  4   �     �     �     �     �     �     �  !     %   =     c     �     �  &   �     �  +   �     �     �     	          )     /     3     F     N     Z                                   	                               !                                               
                                              Actions Auto Automatically turn pages Big Calaméo publication widget Default page Display the publication title Enter the page number to display by default Full Medium Mini Mode More options Open description page Open in new window Open publication in full screen directly Open viewer directly Publication Publications Read more publications on Calaméo Scroll Show the sharing menu after reading Size Slide Small The URL of the publication Title URL View Widgets like a bookPublication nounBook Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Actions Auto Tourner les pages automatiquement Grand Widget Calaméo Page par défaut Afficher le titre de la publication Entrez le numéro de la page à afficher par défaut Pleine largeur Moyen Mini Mode Plus d’options Ouvrir la page de description Ouvrir dans une nouvelle fenêtre Ouvrir la publication en plein écran Ouvrir directement la liseuse Publication Publications Lire plus de publications sur Calaméo Défiler Afficher le menu de partage après avoir lu Taille Diapositive Petit L’URL de la publication Titre URL Mode d’affichage Widgets Publication Livre 